FROM docker.jala.pro/devops/openjdk:8-jdk-alpine
WORKDIR /app

# Copy Gradle project
COPY build.gradle .
COPY src src/
COPY gradle gradle/
COPY gradlew .
COPY mvnw .
COPY pom.xml .

# Build Gradlew Java project.
ENTRYPOINT ["./gradlew","clean","test","--rerun-tasks","--console","verbose"]
